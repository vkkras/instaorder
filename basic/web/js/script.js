jQuery('#instaurl').bind('paste', function(e) {
    jQuery('.pre-order').html('<p>Минуту, идет проверка...</p>');
    jQuery('.pre-order').css({'display': 'block'});
    jQuery('#order_form').css({'display': 'none'});
    setTimeout(function () {
        var instagram = e.target.value;
        var _csrf = $('meta[name=csrf-token]').attr('content');
        var data = {
                instagram: instagram,
                _csrf : _csrf
        };
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/site/instagram-info',
            data: data,
            }).done(function(result) {
                
                if (result.profile_name == 'incorrect') jQuery('.pre-order').html('<p>Некорректный Instagram аккаунт</p>');
                    else {
                        jQuery('.pre-order').html('<img class="profile_pic" src="' + result.profile_pic_url + '" /><a class="profile_link" target="_blank" href="https://www.instagram.com/' + result.profile_name + '/">@' + result.profile_name + '</a>');
                        jQuery('#order_form').css({'display': 'block'});
                        jQuery('input[name="OrderForm[link]"]').val('https://www.instagram.com/' + result.profile_name + '/');
                        jQuery('input[name="OrderForm[profile_picture]"]').val(result.profile_pic_url);
                        jQuery('input[name="OrderForm[profile_name]"]').val(result.profile_name);
                        var price = jQuery('input[name="OrderForm[service]"]:checked').attr('data-price');
                        jQuery('input[name="OrderForm[price]"]').val(price);
                    }
                console.log('success');
            }).fail(function() {
                console.log('fail');
        });
    }, 100);
});

$('input[name="OrderForm[service]"]').click(function (){
    var new_price = jQuery('input[name="OrderForm[service]"]:checked').attr('data-price');
    jQuery('input[name="OrderForm[price]"]').val(new_price);
});