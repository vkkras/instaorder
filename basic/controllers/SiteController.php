<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\OrderForm;
use app\models\Service;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionInstagramInfo()
    {
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $instagram = trim($data['instagram']);
            
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            $is_instagram = strpos($instagram, 'https://www.instagram.com/');
            
            if ($is_instagram === false) {
                return [
                    'profile_pic_url' => '',
                    'profile_name' => 'incorrect',
                ];
            }
            
            if (substr($instagram, -1, 1) != '/') $instagram = $instagram.'/';
            
            $url = 'http://officejl5.beget.tech/proxy.php?url='.$instagram.'?__a=1';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            
            $result = curl_exec($ch);
            curl_close($ch);
            
            $profile = json_decode($result, true);

            $profile_pic_url = $profile['graphql']['user']['profile_pic_url'];
            
            $regex = '/(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_\.]+)/im';
            if (preg_match( $regex, $instagram, $matches ) ) $profile_name = $matches[1];
            
            return [
                'profile_pic_url' => $profile_pic_url,
                'profile_name' => $profile_name,
            ];
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new OrderForm();
        $services = Service::find()->asArray()->all();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $model->operation_id = random_int(99, 999999);
            $result = $model->sendOrder();
            
            return $this->render('order', ['order' => $model, 'result' => $result]);
        }
        
        return $this->render('index', ['services' => $services, 'order' => $model]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
