<?php
$this->title = 'Заказ #'.$order->operation_id;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h1>Номер заказа #<?=$order->operation_id;?></h1>
                <p><img class="profile_pic" src="<?=$order->profile_picture;?>" /><a class="profile_link" target="_blank" href="https://www.instagram.com/<?=$order->profile_name;?>/">@<?=$order->profile_name;?></a> <span class="price_info"><?=$order->quantity;?> из 10000&nbsp;&nbsp;&nbsp;<?=$order->price*$order->quantity;?> руб.</span></p>
                <p>Ответ внешнего провайдера: <?=$result;?></p>
            </div>
        </div>

    </div>
</div>
