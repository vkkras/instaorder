<?php
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <p><strong>Вставьте ссылку на Instagram аккаунт. Пример: https://www.instagram.com/redbull/</strong></p>
                <div class="form-group">
                    <input type="text" class="form-control" name="instaurl" id="instaurl">
                </div>
                <div class="pre-order"></div>
                
                <div id="order_form">
                
                    <?php $form = ActiveForm::begin(['id' => 'order-form']); ?>
                        
                        <div class="form-group">
                        <?php $i = 1;
                        if ($services) foreach ($services as $service ) { ?>
                            <p><input name="OrderForm[service]" type="radio" data-price="<?=$service['rate']?>" value="<?=$service['id']?>"<?php if ($i == 1) echo 'checked';?>> <?=$service['name']?> (<?=$service['rate']?> руб. за 1000)</p>
                        <?php $i++; } ?>
                        </div>
                        
                        <?= $form->field($order, 'price')->hiddenInput()->label(false) ?>
                        
                        <?= $form->field($order, 'quantity')->textInput(['placeholder' => 'Укажите необходимое количество подписчиков', 'type' => 'number', 'min' => 10])->label(false) ?>
                        
                        <?= $form->field($order, 'profile_picture')->hiddenInput()->label(false) ?>
                        
                        <?= $form->field($order, 'profile_name')->hiddenInput()->label(false) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'order-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                    
                </div>
            </div>
        </div>

    </div>
</div>
