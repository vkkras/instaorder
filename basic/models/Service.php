<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $category
 * @property int $rate
 * @property int $min
 * @property int $max
 * @property int $dripfeed
 * @property int $average_time
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'category', 'rate', 'min', 'max'], 'required'],
            [['rate', 'min', 'max', 'dripfeed', 'average_time'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 50],
            [['category'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'category' => 'Category',
            'rate' => 'Rate',
            'min' => 'Min',
            'max' => 'Max',
            'dripfeed' => 'Dripfeed',
            'average_time' => 'Average Time',
        ];
    }
}
