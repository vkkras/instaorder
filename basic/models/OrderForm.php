<?php

namespace app\models;

use Yii;
use yii\base\Model;

class OrderForm extends Model
{
    public $quantity;
    public $service;
    public $price;
    public $profile_picture;
    public $operation_id;
    public $profile_name;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['quantity'], 'required'],
            [['service', 'price'], 'integer'],
            [['quantity'], 'number', 'min' => 10, 'max' => 10000],
            [['operation_id', 'profile_name'], 'string', 'max' => 200],
            [['profile_picture'], 'string', 'max' => 500],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'quantity' => 'Количество',
        ];
    }
    
    public function sendOrder() {
        
        $post_data = '{
                "link": "https://www.instagram.com/'.$this->profile_name.'/",
                "quantity": '.$this->quantity.',
                "service": "'.$this->service.'",
                "operation_id": "'.$this->operation_id.'"
            }';
        
        $url = 'http://45.147.176.76:8910/v1/just/add?action=add&key=911863916d19759ccc873d427e563f223f0ee099de9b47997784eb9aeb03e06d';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
